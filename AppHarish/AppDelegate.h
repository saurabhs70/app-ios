//
//  AppDelegate.h
//  AppHarish
//
//  Created by saurabh-pc on 19/12/17.
//  Copyright © 2017 saurabh-pc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import "ViewController2.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;


@end

